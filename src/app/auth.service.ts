import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';





@Injectable({
  providedIn: 'root'
})

export class AuthService {
  
signup(email:string,password:string){
  return this.fireBaseAuth
   .auth
   .createUserWithEmailAndPassword(email,password);
 }

 user: Observable<firebase.User>;
 updateProfile(user,name:string) {
  user.updateProfile({displayName: name, photoURL: ''});
}

isAuth()
  {
    return this.fireBaseAuth.authState.pipe(map(auth => auth));
  }


  constructor(private fireBaseAuth:AngularFireAuth) { 
    this.user = fireBaseAuth.authState;

  }
}

