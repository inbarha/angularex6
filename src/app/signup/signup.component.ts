import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {FormControl, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';


@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  email: string;
  name: string;
  password: string; 
  code = '';
  message ='';
  wrong = "";
  result: boolean;


  signup(){
    this.wrong = "";
    this.result = /^[a-z0-9]+$/i.test(this.password);
    if (this.result) {
      this.wrong = "Password must contain a symbol ($,#, etc.)";
      return
    }
    // console.log("sign up clicked" +' '+ this.email+' '+this.password+' '+this.name)
     this.authService.signup(this.email,this.password)
       .then(value=>{
          this.authService.updateProfile(value.user,this.name);
        }).then(value => {
          this.router.navigate(['/welcome']); 
       }).catch(err => {
         this.code = err.code;
         this.message = err.message;
         console.log(err);
       })
   }
  
 

  constructor(private authService:AuthService,private router:Router,public afAuth: AngularFireAuth) { }

  ngOnInit() {
  
  
  }
  onLoginGoogle()
  {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    this.router.navigate(['/welcome']);
  }

  onLoginFacebook()
  {
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
    this.router.navigate(['/welcome']);
  }

}



